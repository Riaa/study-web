<link rel="stylesheet" href="style2.css">
<ul>
  <li><a href="index.php">Home</a></li>
  <li><a href="image insert/index.php">Images</a></li>
  <li><a href="upload video/index.php">Videos</a></li>
  <li><a href="upload file/index.php">Software</a></li>
  <li><a href="upload file/index.php">Projects</a></li>
  <li style="float:right"><a class="active" href="#about">About</a></li>
    <li style="float:right"><a class="active" href="#about">Contact</a></li>

<?php
include_once 'dbconfig.php';
?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>File Uploading With PHP and MySql</title>
<link rel="stylesheet" href="style.css" type="text/css" />
</head>
<body>
<div id="header">
<label>File Uploading With PHP and MySql</label>
</div>
<div id="body">
 <form action="upload.php" method="post" enctype="multipart/form-data">
 <input type="file" name="file" />
 <button type="submit" name="btn-upload">upload</button>
 </form>
    <br /><br />
    <?php
 if(isset($_GET['success']))
 {
  ?>
        <label>File Uploaded Successfully...  <a href="view.php">click here to view file.</a></label>
        <?php
 }
 else if(isset($_GET['fail']))
 {
  ?>
        <label>Problem While File Uploading !</label>
        <?php
 }
 else
 {
  ?>
        <label>Try to upload any files(PDF, DOC, EXE, VIDEO, MP3, ZIP,etc...)</label>
        <?php
 }
 ?>
</div>
<div id="footer">
<label><label>
</div>
</body>
</html>